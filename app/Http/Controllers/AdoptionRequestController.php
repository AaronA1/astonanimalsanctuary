<?php
/**
 * Created by PhpStorm.
 * User: Aaron Adejumo
 * Date: 25/03/19
 * Time: 00:22
 */

namespace App\Http\Controllers;

use App\AdoptionRequest;
use App\Animal;
use App\User;
use App\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdoptionRequestController extends Controller
{

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // If user is not an admin, they can not visit the adoption request page, they are redirected.
        if (Gate::denies('isAdmin')) {
            return redirect('/home');
        }

        // Eloquent queries into Adoption Request table in database and retrieves all records.
        $adoptions = AdoptionRequest::all();
        // Reverse requests so that most recent is first.
        $adoptions = $adoptions->reverse();

        $animals = Animal::all();
        $people = User::all();
        $images = Image::all();
        return view('requests.index', compact('adoptions', 'animals', 'people', 'images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Get the animals' id from the HTTP request
        $animid = $request->input('animal');

        // Find all requests this user has made for this animal
        $allrequests = AdoptionRequest::all();
        $reqforthisanim = $allrequests->where('request_for',$animid);
        $reqbythisuser = $reqforthisanim->where('request_by',Auth::user()->id);

        // If the collection is not empty (they already made a request) then they cannot make another one
        if ($reqbythisuser->isNotEmpty()) {
            return back()->withErrors('You have already sent a request for this animal');
        }

        // Construct a new Adoption Request
        $adoption = new AdoptionRequest;

        // Set the respective adoption request field to the value
        $adoption->request_for = $animid;
        $adoption->request_by = Auth::user()->id;

        // Save the adoption request in the table
        $adoption->save();

        return back()->with('success', 'Your adoption request has been received!');
    }

    /**
     * Update the specified resource in storage by approving the request
     *
     * @param Request $request
     * @param  Animal  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // Find the relevant adoption request by the id
        $adoption = AdoptionRequest::find($id);

        // If the approve button was pressed
        if ($request->get('decision') == 'approve') {

            // Find the animal they requested
            $animal = Animal::find($adoption->request_for);

            // If the animal has already been adopted after the request has been made,
            if ($animal->adopted == 1) {
                return back()->withErrors('This animal has already been adopted');
            }

            // Change adoption status and animals details
            $adoption->status = 'Approved';
            $animal->adopted = 1;
            $animal->adopted_by = $adoption->request_by;
            $animal->save();

        } else {
            $adoption->status = 'Denied';
        }

        // Re-save the request
        $adoption->save();

        return redirect('requests')->with('success', 'This request has been dealt with');
    }
}