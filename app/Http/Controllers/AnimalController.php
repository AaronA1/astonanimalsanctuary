<?php

namespace App\Http\Controllers;

use App\AdoptionRequest;
use Illuminate\Support\Facades\Gate;
use App\Animal;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AnimalController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $animals = Animal::all();
        $users = User::all();
        if (Gate::denies('isAdmin')) {
            $animals = $animals->where('adopted',0);
        }
        return view('animals.index', array('animals'=>$animals), compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // If user is not an admin, they can not visit the creation page, they are redirected.
        if (Gate::denies('isAdmin')) {
            return redirect('animals');
        }
        return view('animals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'type' => 'required',
            'dob' => 'required|date',
            'description' => 'required|string|max:256',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:500'
        ]);

        // Construct and store the animal using the data from the form
        $animal = new Animal;
        $animal->name = $request->input('name');
        $animal->type = $request->input('type');
        $animal->dob = $request->input('dob');
        $animal->description = $request->input('description');
        $animal->created_at = now();
        $animal->save();

        // Construct and store the image for the animal, since images are required.
        $fileNameWithExt = $request->file('image')->getClientOriginalName();
        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        $image = new Image;
        $image->path = $fileNameToStore;
        $image->created_at = now();
        $image->id = $animal->id;
        $image->save();

        return back()->with('success', 'New potential pet has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  Animal  $id
     * @return Response
     */
    public function show($id)
    {
        $animal = Animal::find($id);
        $images = Image::where('id',$id)->get();
        $owner = User::where('id',$animal->adopted_by)->first();
        return view('animals.show', compact('animal', 'images', 'owner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Animal  $id
     * @return Response
     */
    public function edit($id)
    {
        if (Gate::denies('isAdmin')) {
            return redirect('animals');
        }
        $animal = Animal::find($id);
        return view('animals.edit', compact('animal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Animal  $id
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $animal = Animal::find($id);
        $this->validate(request(), [
            'description' => 'sometimes|string|max:256',
            'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:500'
        ]);
        if ($request->hasFile('image')){

            $fileNameWithExt = $request->file('image')->getClientOriginalName();

            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('image')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);

            $image = new Image;
            $image->id = $animal->id;
            $image->path = $fileNameToStore;
            $image->created_at = now();
            $image->save();
        }

        $animal->description = $request->input('description');
        $animal->updated_at = now();
        $animal->save();

        return redirect('animals')->with('success','Animal has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Animal  $id
     * @return Response
     */
    public function destroy($id)
    {
        $animal = Animal::find($id);
        $animal->delete();
        return redirect('animals')->with('success','Animal successfully deleted');
    }
}
