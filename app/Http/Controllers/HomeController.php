<?php

namespace App\Http\Controllers;

use App\AdoptionRequest;
use App\Animal;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $adoptions = AdoptionRequest::all();
        $adoptions = $adoptions->reverse();
        if (Gate::denies('isAdmin')) {
            $adoptions = $adoptions->where('request_by', Auth::user()->id);
        }
        return view('home', compact('adoptions'));
    }

    public function show() {

        if (Gate::allows('isAdmin')) {
            return redirect('home');
        }
        $pets = Animal::all();
        $pets = $pets->where('adopted_by', Auth::user()->id);
        $images = Image::all();
        return view('mypets', compact('pets', 'images'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request) {
        $this->validate(request(), [
            'about' => 'sometimes|string|max:256',
            'avatar' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:500'
        ]);

        $user = Auth::user();

        if ($request->hasFile('avatar')) {

            $fileNameWithExt = $request->file('avatar')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('avatar')->storeAs('public/images', $fileNameToStore);
            $user->avatar = $fileNameToStore;
        }

        $user->about = $request->get('about');
        $user->save();

        return back()->with('success', 'Profile successfully updated');
    }

}
