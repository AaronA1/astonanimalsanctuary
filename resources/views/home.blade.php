@extends('layouts.app')
@section('content')
    <title>My Dashboard</title>
    <div class="container">
        @if (\Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ \Session::get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (Gate::denies('isAdmin'))
                            <div>
                                <h>The status of your previous adoption requests: <i>(most recent first)</i></h>
                                <ul>
                                    @foreach ($adoptions as $adoption)
                                        <li>{{$adoption->status}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Gate::allows('isAdmin'))
                            <div>
                                @foreach ($adoptions as $adoption)
                                    @if ($adoption->status == "Pending")
                                        <a href="{{url('requests')}}">There are unresolved adoption requests!</a>
                                        @break
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                <a href="{{url('animals/create')}}">Add a new companion</a>
                            </div>
                        @endif
                        <div>
                            <a href="" data-toggle="modal" data-target="#editModal">Update your profile?</a>
                        </div>
                        <div>
                            <a href="{{url('animals')}}">Browse our animals!</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" align="middle">
                    <img src="{{asset('storage/images/'.Auth::user()->avatar)}}" style="width:100%">
                    <h2>{{Auth::user()->name}} {{Auth::user()->surname}}</h2>
                    <h6>{{Auth::user()->email}}</h6>
                    <h9>{{Auth::user()->about}}</h9>
                </div>
            </div>
        </div>
    </div>

    <!-- POPUP -->
    <div class="modal fade" id="editModal" tabindex="" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-edit">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h6 class="text-muted">Edit your profile</h6>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="POST" action="{{action('HomeController@update')}}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label>Profile Picture:</label><br>
                            <input type="file" name="avatar"/>
                        </div>
                        <div class="form-group">
                            <label>About You:</label>
                            <textarea rows="4" class="form-control" name="about">{{Auth::user()->about}}</textarea>
                        </div>
                        <button class="btn btn-block btn-round">Update</button>
                </div>
            </div>
        </div>
    </div>
@endsection
