@extends('layouts.app')
@section('content')
    <title>My Pets</title>
    <div class="container">
        @if( \Session::has('success') )
            <div class="alert alert-success">
                {{ \Session::get('success') }}
            </div>
        @endif
        @if ($pets->isEmpty())
            <div align="middle">
                <h1>You haven't adopted any pets :(</h1>
                <a href="{{url('animals')}}"><h2>Get started here!</h2></a>
            </div>
        @endif
        <div class="row">
            @foreach($pets as $pet)
                @php
                    $images = $images->where('id',$pet->id);
                @endphp
                <div class="col-md-12">
                    <div class="card">
                        <div align="middle" class="card-header"><strong>{{$pet->name}}</strong></div>
                        <div class="card-body">
                            <div class="row">
                                @foreach($images as $image)
                                    <div class="col-md-3">
                                        <img id="image" width="100%" height="200px" src="{{asset('storage/images/'.$image->path)}}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection