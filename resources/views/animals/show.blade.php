@extends('layouts.app')
@section('content')
    <style>
        /* The Modal (background) */
        .modal
            display: none;
            position: fixed;
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: black;
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            margin: auto;
            padding: 0;
            max-width: 1200px;
        }

        /* The Close Button */
        .close {
            color: white;
            position: absolute;
            top: 10px;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
        }

        /* Hide the slides by default */
        .mySlides {
            display: none;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #ff4500;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }
    </style>
    <title>{{$animal->name.' - Aston Animal Sanctuary'}}</title>
    <div class="container">
        <!-- display the errors -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        <div class="row">
            <div class="col-md-8">
            <table class="table table-striped">
                <tr> <th>Name</th> <td>{{$animal->name}}</td></tr>
                <tr> <th>Animal type </th> <td>{{$animal->type}}</td></tr>
                <tr> <th>Date of Birth </th> <td>{{$animal->dob}}</td></tr>
                <tr> <th>Notes: </th> <td style="max-width:150px;" >{{$animal->description}}</td></tr>
                <tr> <th>Available for adoption: </th> <td id="adopted">{{$animal->adopted}}</td></tr>
                @if ($owner != null)
                    <tr> <th>Adopted By: </th> <td id="adopted">{{$owner->name}}</td></tr>
                @endif
                @if($animal->adopted == "0")
                    <script>document.getElementById("adopted").innerText = "Yes";</script>
                @else
                    <script>document.getElementById("adopted").innerText = "No";</script>
                @endif
            </table>
            <table>
                <tr>
                    <td><a href="{{ url('animals') }}" class="btn btn-primary" role="button">Back to the list</a></td>
                    @if (Gate::denies('isAdmin'))
                        <td>
                            <form action="{{action('AdoptionRequestController@store')}}"
                                  method="post">
                                @csrf
                                <input name="animal" type="hidden" value="{{$animal['id']}}">
                                <button class="btn btn-success" type="submit">Apply to Adopt</button>
                            </form>
                        </td>
                    @else
                        <td><a href="{{action('AnimalController@edit', $animal['id'])}}" class="btn btn-warning">Edit</a></td>
                        <td>
                            <form action="{{action('AnimalController@destroy', $animal['id'])}}"
                                  method="post"> @csrf
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    @endif
                </tr>
            </table>
            </div>
            <div class="col-md-4">
                <div class="card" align="middle">
                    <img onclick="openModal()" src="{{asset('storage/images/'.$images[0]->path)}}" style="width:100%">
                    <h4>{{$animal->name}}</h4>
                </div>
            </div>
            <!-- Lightbox Image Gallery popup -->
            <div id="myModal" class="modal">
                <span class="close cursor" onclick="closeModal()">&times;</span>
                <div class="modal-content">
                    @for ($num = 0; $num < $images->count(); $num++)
                        <div class="mySlides">
                            <div class="numbertext">{{$num+1}} / {{$images->count()}}</div>
                            <img src="{{asset('storage/images/'.$images[$num]->path)}}" style="width:500px;height:500px">
                        </div>
                    @endfor

                <!-- Next/previous controls -->
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>
                </div>
            </div>
        </div>
    </div>
    <script>
        /*Javascript functions for various things*/

        // Open the Modal
        function openModal() {
            document.getElementById('myModal').style.display = "block";
        }

        // Close the Modal
        function closeModal() {
            document.getElementById('myModal').style.display = "none";
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            captionText.innerHTML = dots[slideIndex-1].alt;
        }
    </script>
@endsection