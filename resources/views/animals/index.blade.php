@extends('layouts.app')
@section('content')
    <title>Here are all our friends</title>
    <div align="middle" class="container">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <input type="text" id="searchTableName" onkeyup="searchTable(0)" placeholder="Search table by pet name">
        <input type="text" id="searchTableType" onkeyup="searchTable(1)" placeholder="Search table by pet type">
        <div class="card-body">
            <table id="animal-table" class="table table-hover">
                <thead>
                <!--onclick property allows user to sort table by clicking the header-->
                <th onclick="sortTable(0)">Name</th>
                <th onclick="sortTable(1)">Type</th>
                <th>Age</th>
                <th onclick="sortTable(2)">Date of Birth</th>
                <th onclick="sortTable(4)">Added on</th>
                @if (Gate::allows('isAdmin'))
                    <th>Owner</th>
                @endif
                <th></th>
                </thead>
                <tbody>
                @foreach($animals as $animal)
                    <tr>
                        <td>{{$animal['name']}}</td>
                        <td>{{$animal['type']}}</td>
                        @php
                            $dob = new DateTime($animal['dob']);
                            $now = new DateTime();
                            $difference = $now->diff($dob);
                            $age = $difference->m;
                            $months = $difference->format('%m') + 12 * $difference->format('%y');
                        @endphp
                        @if ($months >= 24)
                            <td>{{$age.'yrs'}}</td>
                        @else
                            <td>{{$months.'mths'}}</td>
                        @endif
                        <td>{{$animal['dob']}}</td>
                        <td>{{$animal['created_at']->toDateString()}}</td>
                        @if (Gate::allows('isAdmin'))
                            @if ($animal->adopted == 1)
                                <td>{{$users->find($animal['adopted_by'])['name']}}</td>
                            @else
                                <td>N/A</td>
                            @endif
                        @endif
                        @guest
                        @else
                            <td><a href="{{action('AnimalController@show', $animal['id'])}}" class="btn btn-primary">Details</a></td>
                        @endguest
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        // Javascript function allowing the user to sort the table by clicking the headers
        function sortTable(n) {
            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("animal-table");
            switching = true;
            //Set the sorting direction to ascending:
            dir = "asc";
            /*Make a loop that will continue until
            no switching has been done:*/
            while (switching) {
                //start by saying: no switching is done:
                switching = false;
                rows = table.rows;
                /*Loop through all table rows (except the
                first, which contains table headers):*/
                for (i = 1; i < (rows.length - 1); i++) {
                    //start by saying there should be no switching:
                    shouldSwitch = false;
                    /*Get the two elements you want to compare,
                    one from current row and one from the next:*/
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    /*check if the two rows should switch place,
                    based on the direction, asc or desc:*/
                    if (dir == "asc") {
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch= true;
                            break;
                        }
                    } else if (dir == "desc") {
                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    /*If a switch has been marked, make the switch
                    and mark that a switch has been done:*/
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    //Each time a switch is done, increase this count by 1:
                    switchcount ++;
                } else {
                    /*If no switching has been done AND the direction is "asc",
                    set the direction to "desc" and run the while loop again.*/
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }

        // Javascript function allowing to search columns of the table (depending on what n is)
        function searchTable(n) {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            if (n == 0) {
                input = document.getElementById("searchTableName");
            } else if (n == 1) {
                input = document.getElementById("searchTableType");
            }
            filter = input.value.toUpperCase();
            table = document.getElementById("animal-table");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[n];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
@endsection