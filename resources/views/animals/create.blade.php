@extends('layouts.app')
@section('content')
    <title>Add a new friend!</title>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 ">
                <!-- display the errors -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
            <!-- display the success status -->
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div><br/>
            @endif
            <!-- define the form -->
                <div class="align-content-center">
                    <form class="form-horizontal" method="POST"
                          action="{{url('animals') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-8">
                            <label>Pet Name</label>
                            <input class="form-control" type="text" name="name"
                                   placeholder="" />
                        </div>
                        <br>
                        <div class="col-md-8">
                            <label>Pet type</label>
                            <select class="form-control" name="type" >
                                <option value="Dog">Dog</option>
                                <option value="Cat">Cat</option>
                                <option value="Fish">Fish</option>
                                <option value="Bird">Bird</option>
                                <option value="Rabbit">Rabbit</option>
                                <option value="Reptile">Reptile</option>
                                <option value="Other">Other...</option>
                            </select>
                        </div>
                        <br>
                        <div class="col-md-8">
                            <label>Date of Birth</label>
                            <br>
                            <input name="dob" type="date"/>
                        </div>
                        <br>
                        <div class="col-md-8">
                            <label >Description</label>
                            <textarea class="form-control" rows="4" cols="50" name="description" placeholder="Describe our friend..."></textarea>
                        </div>
                        <br>
                        <div class="col-md-8">
                            <label>Image</label>
                            <input type="file" name="image"
                                   placeholder="Image file..." multiple/>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary" />
                            <input type="reset" class="btn btn-outline-default" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        label {
            font-weight: bold;
        }
    </style>
@endsection