<!DOCTYPE html>
<head lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/css/paper-kit.css?v=2.1.0')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/demo.css')}}" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{asset('assets/css/nucleo-icons.css')}}" rel="stylesheet" />
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('AAS', 'AAS') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @guest
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('home') }}">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('animals') }}">Animals</a>
                    </li>
                    @if (Gate::allows('isAdmin'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('animals/create') }}">New Pet</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('requests') }}">Requests</a>
                        </li>
                    @elseif (Gate::denies('isAdmin'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('mypets') }}">My Pets</a>
                        </li>
                    @endif
                @endguest
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">Logout</a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

{{--<div id="myNav" class="overlay">--}}
{{--<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>--}}
{{--<div class="overlay-content">--}}
{{--<a href="{{ url('animals') }}">List</a>--}}
{{--<a href="{{ url('animals/create') }}">Create</a>--}}
{{--<a href="{{ url('requests') }}">Requests</a>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div style=";background-color: rgba(0,0,0,0.8);text-align:center;font-size:20px;cursor:pointer" onclick="openNav()">N A V I G A T &#9776;</div>--}}

{{--<script>--}}
{{--function openNav() {--}}
{{--document.getElementById("myNav").style.height = "100%";--}}
{{--}--}}
{{--function closeNav() {--}}
{{--document.getElementById("myNav").style.height = "0%";--}}
{{--}--}}

{{--</script>--}}

<main class="py-4">
    @yield('content')
</main>
</body>
<footer>
    <!-- Core JS Files -->
    <script src="{{asset('assets/js/jquery-3.2.1.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <!-- Switches -->
    <script src="{{asset('assets/js/bootstrap-switch.min.js')}}"></script>

    <!--  Plugins for Slider -->
    <script src="{{asset('assets/js/nouislider.js')}}"></script>

    <!--  Paper Kit Initialization and functions -->
    <script src="{{asset('assets/js/paper-kit.js?v=2.1.0')}}"></script>
</footer>
</lang>
</html>
