@extends('layouts.app')
@section('content')
    <div class="container">
        @if( \Session::has('success') )
            <div class="alert alert-success">
                {{ \Session::get('success') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        @if ($adoptions->isEmpty())
            <div align="middle">
                <h1>There are no adoption requests</h1>
                <a href="{{url('animals/create')}}"><h2>Maybe add a new pet?</h2></a>
            </div>
        @endif
        <div class="row">
            @foreach($adoptions as $adoption)
                @php
                    $animal = $animals->where('id',$adoption->request_for)->first();
                    $person = $people->where('id',$adoption->request_by)->first();
                    $image = $images->where('id',$animal->id)->first();
                @endphp
                <div class="col-md-4 ">
                    <div class="card">
                        <div align="middle" class="card-header"><strong>{{$animal->name}}</strong></div>
                        <div class="card-body">
                            <div align="middle"><a href="{{url('animals/'.$animal->id)}}"><img id="image" width="200px" height="200px" src="{{asset('storage/images/'.$image->path)}}"></a></div>
                            <div>
                                <table class="table table-hover table-borderless">
                                    <tr> <th>Applicant</th> <td>{{$person->name}}</td></tr>
                                    <tr> <th>Status</th> <td>{{$adoption->status}}</td></tr>
                                </table>
                            </div>
                            <div align="middle">
                                @if (Gate::allows('isAdmin'))
                                    @if ($adoption->status == 'Pending' /*&& $animal->adopted == 0*/)
                                        <td>
                                            <form class="form-horizontal" action="{{action('AdoptionRequestController@update', $adoption['id'])}}"
                                                  method="POST">
                                                @method('PATCH')
                                                @csrf
                                                <input type="hidden">
                                                <button name="decision" class="btn btn-success" type="submit" value="approve">Approve</button>
                                                <button name="decision" class="btn btn-danger" type="submit" value="deny">Deny</button>
                                            </form>
                                        </td>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection