<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 10);
            $table->enum('type',['Dog','Cat','Fish','Bird','Rabbit','Reptile','Other']);
            $table->date('dob');
            $table->string('description', 256);
            $table->boolean('adopted')->default(0);
            $table->unsignedBigInteger('adopted_by')->nullable();
            $table->timestamps();

            $table->foreign('adopted_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
